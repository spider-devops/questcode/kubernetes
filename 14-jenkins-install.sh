kubectl apply -f 13-jenkins-pv-pvc.yaml

helm install jenkins --set persistence.existingClaim=jenkins --set master.serviceType=NodePort --set master.nodePort=31090 -n devops stable/jenkins

kubectl create rolebinding sa-jenkins-role-clusteradmin --clusterrole=cluster-admin --serviceaccount=jenkins:default --namespace=jenkins

kubectl create rolebinding sa-jenkins-role-clusteradmin-kubesystem --clusterrole=cluster-admin --serviceaccount=jenkins:default --namespace=kube-system   

kubectl create rolebinding sa-jenkins-role-clusteradmin-production --clusterrole=cluster-admin --serviceaccount=jenkins:default --namespace=production   
